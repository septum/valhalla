/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.valhalla.utils.base64;

import java.util.Base64;

/**
 *
 * @author marsa
 */
public class Base64Utils {

	public static String encode(final String input) {
		return Base64.getUrlEncoder().withoutPadding().encodeToString(input.getBytes());
	}

	public static String encode(final byte[] input) {
		return Base64.getUrlEncoder().withoutPadding().encodeToString(input);
	}

	
	public static String decode(final String input) {
		return new String(Base64.getDecoder().decode(input));
	}
}
