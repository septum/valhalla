package es.valhalla.exception;

public class ValhallaError {

	private String message;
	private String code;
	private Throwable exception;
	
	
	public ValhallaError(String message, String code ) {
		super();
		this.message = message;
		this.code = code;
	}

	public ValhallaError(String message, String code, Throwable exception) {
		super();
		this.message = message;
		this.code = code;
		this.exception = exception;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Throwable getException() {
		return exception;
	}

	public void setException(Throwable exception) {
		this.exception = exception;
	}

}
