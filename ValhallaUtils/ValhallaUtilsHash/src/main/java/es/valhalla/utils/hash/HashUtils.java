/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.valhalla.utils.hash;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import es.valhalla.utils.hash.constants.HashConstants;
import es.valhalla.utils.hash.exception.HashExceptionEnum;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;

/**
 *
 * @author marsa
 */
public class HashUtils {
	
	
	private final static ValhallaLogger LOGGER = ValhallaLoggerImpl.getInstance(HashUtils.class);
	
	public static byte[] generateSalt() {
		SecureRandom random = new SecureRandom();
		byte[] salt = new byte[16];
		random.nextBytes(salt);
		return salt;
	}
	

	public static byte[] hashPBKDF2(String data, byte[] salt) {
		try {
	
			final KeySpec spec = new PBEKeySpec(data.toCharArray(), salt, HashConstants.SALT_COUNT, HashConstants.ITERATIONS);
			final SecretKeyFactory factory = SecretKeyFactory.getInstance(HashConstants.FACTORY_PBKDF2);
			final byte[] hash = factory.generateSecret(spec).getEncoded();
			return hash;
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error("AlgorithmException {}", e);
			throw HashExceptionEnum.NO_SUCH_ALGO.getException(e);
		}catch( InvalidKeySpecException e) {
			LOGGER.error("InvalidKeySpecException {}", e);
			throw HashExceptionEnum.INVALID_KEY.getException(e);

		}
	}

}
