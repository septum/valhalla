/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.valhalla.utils.hash.constants;

/**
 *
 * @author marsa
 */
public class HashConstants {
    
    public static final String FACTORY_PBKDF2="PBKDF2WithHmacSHA1";
    public static final int SALT_COUNT=65536;
    public static final int ITERATIONS=128;
}
