package es.valhalla.utils.hash.exception;

import es.valhalla.exception.ValhallaError;

public enum HashExceptionEnum {

	NO_SUCH_ALGO("valhallashash0001", "Error Algorithm not found"),
	INVALID_KEY("valhallashash0002", "Invalid key");

	private String code;
	private String message;

	private HashExceptionEnum(final String code, final String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public HashException getException(final Throwable throwable) {
		final ValhallaError error = new ValhallaError(code, message, throwable);
		final HashException exception = new HashException(error);
		return exception;
	}

}
