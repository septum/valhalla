package es.valhalla.utils.configuration.application.service.Impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.enterprise.context.ApplicationScoped;

import es.valhalla.utils.configuration.application.service.ConfigurationApplicationService;
import es.valhalla.utils.configuration.application.service.ConfigurationApplicationServiceConstants;
import es.valhalla.utils.configuration.application.service.exception.ConfigurationApplicationServiceExceptionEnum;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;

@ApplicationScoped
public class ConfigurationApplicationServiceImpl implements ConfigurationApplicationService {

	private Map<Object, Object> appPropsCached = new ConcurrentHashMap<>();

	private ValhallaLogger LOGGER = ValhallaLoggerImpl.getInstance(ConfigurationApplicationServiceImpl.class);


	private Properties appProps;

	@Override
	public int getIntProperty(final String appName, final String property) {
		final int propertyValue = Integer.valueOf(getProperty(appName, property));
		return propertyValue;
	}

	@Override
	public double getDoubleProperty(final String appName, final String property) {
		final double propertyValue = Double.valueOf(getProperty(appName, property));
		return propertyValue;
	}

	@Override
	public String getStringProperty(final String appName, final String property) {
		final String propertyValue = getProperty(appName, property);
		return propertyValue;
		}

	@Override
	public boolean getBooleanProperty(final String appName, final String property) {
		final boolean propertyValue = Boolean.valueOf(getProperty(appName, property));
		return propertyValue;
	}

	private String getProperty(final String appName, final String propertyName) {
		String property = (String) appPropsCached.get(propertyName);

		if (property == null) {
			property = readProperties(appName).getProperty(propertyName);
		}

		return property;
	}

	@Override
	public Properties readProperties(final String appName) {

		final String propFileName = appName.concat(".properties");
		try {
			appProps = appProps == null ? new Properties() : appProps;

			final String argumentPropertyPath= System.getProperty(ConfigurationApplicationServiceConstants.ENV_CONF_PROPERTY);	
			
			if(argumentPropertyPath!=null) {
				LOGGER.info("config loaded from {}", argumentPropertyPath);
			}
			
			final InputStream inputStream =argumentPropertyPath!=null ? new FileInputStream(new File(argumentPropertyPath.concat("/").concat(propFileName))) : getClass().getClassLoader().getResourceAsStream(propFileName);

			if (inputStream != null) {
				appProps.load(inputStream);
				appProps.forEach((key, value) -> {
					appPropsCached.put(key, value);
				});
			} else {
				throw ConfigurationApplicationServiceExceptionEnum.PROPERTIES_NOT_FOUND.getException(new FileNotFoundException());
			}

		} catch (IOException e) {
			LOGGER.error("Propertie file {} not found", e, propFileName);
			throw ConfigurationApplicationServiceExceptionEnum.PROPERTIES_NOT_FOUND.getException(e);

		}
		return appProps;
	}
	
	
	

}
