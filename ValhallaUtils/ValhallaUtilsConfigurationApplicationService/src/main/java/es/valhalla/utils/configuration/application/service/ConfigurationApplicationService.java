package es.valhalla.utils.configuration.application.service;

import java.util.Properties;

public interface ConfigurationApplicationService {

	public int getIntProperty(String appName, String property);

	public double getDoubleProperty(String appName, String property);

	public String getStringProperty(String appName, String property);

	public boolean getBooleanProperty(String appName, String property);

	Properties readProperties(String appName);

}
