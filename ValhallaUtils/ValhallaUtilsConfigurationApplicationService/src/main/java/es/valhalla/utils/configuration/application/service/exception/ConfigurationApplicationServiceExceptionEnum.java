package es.valhalla.utils.configuration.application.service.exception;

import es.valhalla.exception.ValhallaError;
import es.valhalla.utils.jsonserializer.exception.JsonSerializerException;
public enum ConfigurationApplicationServiceExceptionEnum {

	
	PROPERTIES_NOT_FOUND("valhallaconfigurationservice0001","Properties not found on the specified path");

	
	private String code;
	private String message;
	
	
	
	private ConfigurationApplicationServiceExceptionEnum(final String code,final String message) {
		this.code = code;
		this.message = message;
	}
	
	
	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public JsonSerializerException getException(final Throwable throwable) {
		final ValhallaError error= new ValhallaError(code, message,throwable);
		final JsonSerializerException exception = new JsonSerializerException(error);
		return exception;
	}
	
}
