package es.valhalla.utils.files.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.enterprise.context.ApplicationScoped;

import es.valhalla.utils.files.service.FileService;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;

@ApplicationScoped
public class FileServiceImpl implements FileService {
	
	
	private ValhallaLogger LOGGER = ValhallaLoggerImpl.getInstance(FileServiceImpl.class);

	@Override
	public void saveFile(final InputStream stream, final String filePath, final String fileName) {

		File directory = new File(filePath);
		if (!directory.exists()) {
			directory.mkdirs();

		}
		int read = 0;
		byte[] bytes = new byte[1024];
		try (OutputStream out = new FileOutputStream(new File(directory + "/" + fileName))) {
			while ((read = stream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
		} catch (IOException e) {

			LOGGER.error("File {} not found in {}", e, fileName, filePath);
		}
	}

	@Override
	public File getFile(final String filePath, final String fileName) {
		File file = new File(filePath + "/" + fileName);
		return file;
	}

	@Override
	public void deleteFile(final String filePath, final String fileName) {
		try {
			Path fileToDeletePath = Paths.get(filePath + "/" + fileName);
			Files.delete(fileToDeletePath);
		} catch (IOException e) {
			LOGGER.error("File {} not found in {}", e, fileName, filePath);
		}

	}
}