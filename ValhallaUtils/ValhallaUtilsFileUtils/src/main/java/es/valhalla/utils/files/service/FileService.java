package es.valhalla.utils.files.service;

import java.io.File;
import java.io.InputStream;

public interface FileService {

	File getFile(String filePath, String fileName);

	void saveFile(InputStream stream, String filePath, String fileName);
	
	void deleteFile(String filePath, String fileName);

}
