package es.valhalla.utils.date.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

	public static String getCurrentDate(final String dateformat) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateformat);
		return sdf.format(new Date());
	}

	public static boolean isDateAfter(final String dateA, final String dateB, final String dateFormat) {

		final SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		try {
			return sdf.parse(dateA).after(sdf.parse(dateB));
		} catch (final ParseException e) {
			return false;
		}
	}

	public static boolean isDateBefore(final String dateA, final String dateB, final String dateFormat) {

		final SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		try {
			return sdf.parse(dateA).before(sdf.parse(dateB));
		} catch (final ParseException e) {
			return false;
		}
	}

}
