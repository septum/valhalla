/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.valhalla.utils.jsonserializer;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.TypeFactory;

import es.valhalla.utils.jsonserializer.exception.JsonSerializerExceptionEnum;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;

/**
 *
 * @author marsa
 */
public class JsonSerializeUtils {

	private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private final static ValhallaLogger LOGGER = ValhallaLoggerImpl.getInstance(JsonSerializeUtils.class);

	static {
		OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		OBJECT_MAPPER.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		OBJECT_MAPPER.setSerializationInclusion(Include.NON_NULL);
	}

	public static String serialize(final Object object) {
		String json = null;
		try {
			json = OBJECT_MAPPER.writeValueAsString(object);
		} catch (JsonProcessingException ex) {
			LOGGER.error("Error serializing {}", ex);
			throw JsonSerializerExceptionEnum.SERIALIZATION_EXCEPTION.getException(ex);
		}
		return json;
	}

	public static <T> T deserialize(final String source, final Class<T> clazz) {
		T object = null;
		try {
			object = OBJECT_MAPPER.readValue(source, clazz);
		} catch (JsonProcessingException ex) {
			LOGGER.error("Error deserializing {}", ex);
			throw JsonSerializerExceptionEnum.DESERIALIZATION_EXCEPTION.getException(ex);
		}
		return object;
	}

	public static <T> T deserialize(final String source, final TypeReference<T> clazz) {
		T object = null;
		try {
			object = OBJECT_MAPPER.readValue(source, clazz);
		} catch (JsonProcessingException ex) {
			LOGGER.error("Error deserializing {}", ex);
			throw JsonSerializerExceptionEnum.DESERIALIZATION_EXCEPTION.getException(ex);

		}
		return object;
	}

	public static <T> List<T> deserializeUntypedList(final String source, final Class<T> clazz) {
		List<T> object = null;
		try {
			TypeFactory t = TypeFactory.defaultInstance();
			object = OBJECT_MAPPER.readValue(source, t.constructCollectionType(ArrayList.class, clazz));
		} catch (JsonProcessingException ex) {
			LOGGER.error("Error deserializing {}", ex);
			throw JsonSerializerExceptionEnum.DESERIALIZATION_EXCEPTION.getException(ex);

		}
		return object;
	}

	public static <T> T convertValue(final Object source, final Class<T> clazz) {
		return OBJECT_MAPPER.convertValue(source, clazz);
	}
}
