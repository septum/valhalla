package es.valhalla.utils.jsonserializer.exception;

import es.valhalla.exception.ValhallaError;

public enum JsonSerializerExceptionEnum {

	SERIALIZATION_EXCEPTION("valhallaserializer0001", "Error while serializing object"),
	DESERIALIZATION_EXCEPTION("valhallaserializer0001", "Error while deserializing to object");

	private String code;
	private String message;

	private JsonSerializerExceptionEnum(final String code, final String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public JsonSerializerException getException(final Throwable throwable) {
		final ValhallaError error = new ValhallaError(code, message, throwable);
		final JsonSerializerException exception = new JsonSerializerException(error);
		return exception;
	}

}
