package es.valhalla.utils.logger;

/**
 *
 * @author marsa
 */
public interface ValhallaLogger {

	public void info(String msg, Object... args);
	
	public void error(String msg,  Object... args);

	public void error(String msg, Throwable t);
	
	public void error(String msg, Throwable t, Object...argd);

}
