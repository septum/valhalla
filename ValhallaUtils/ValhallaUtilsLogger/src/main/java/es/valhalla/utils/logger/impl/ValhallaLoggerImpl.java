/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.valhalla.utils.logger.impl;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import es.valhalla.utils.logger.ValhallaLogger;

/**
 *
 * @author marsa
 */
public class ValhallaLoggerImpl implements  ValhallaLogger{

	private Logger LOGGER;

	
	private <T> ValhallaLoggerImpl(final Class<T> source) {
		LOGGER= LogManager.getLogger(source);
	}

	public static <T> ValhallaLoggerImpl getInstance(Class<T> clazz) {
		
		return new ValhallaLoggerImpl(clazz);
	}
	

	@Override
	public void info(final String message,final Object...args) {
		LOGGER.log(Level.INFO, message, args);
	}

	@Override
	public void error(final String msg,final Throwable t) {
		LOGGER.log(Level.ERROR, msg, t);
	}
	
	@Override
	public void error(final String msg,final Throwable t,final  Object...args) {
		LOGGER.log(Level.ERROR, msg, t,args);
	}

	@Override
	public void error(String msg, Object... args) {
		LOGGER.log(Level.ERROR, msg,args);
		
	}

}
