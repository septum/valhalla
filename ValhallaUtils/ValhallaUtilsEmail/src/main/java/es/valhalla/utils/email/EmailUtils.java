package es.valhalla.utils.email;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailUtils {
	

	public static void sendEmail(final String recipientEmail,final String subject,final String contentText, final Properties properties){
		try {
			
			Session session = Session.getDefaultInstance(properties);
			MimeMessage message = new MimeMessage(session);

			message.setFrom(new InternetAddress((String) properties.getProperty(EmailConstants.SMTP_SENDER)));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipientEmail));
			message.setSubject(subject);
			message.setText(contentText);
			Transport t = session.getTransport(EmailConstants.SMTP_PROTOCOL);

			t.connect((String) properties.get(EmailConstants.SMTP_USER), (String)properties.getProperty(EmailConstants.SMTP_PASSWORD));
			t.sendMessage(message, message.getAllRecipients());
			t.close();

		} catch (MessagingException me) {
			me.printStackTrace();
			throw new RuntimeException("error en crear/empaquetar/enviar Email");
		}
	}
}
