package es.valhalla.utils.email;

public class EmailConstants {
	
	public final static String SMTP_SENDER="mail.smtp.mail.sender";
	public final static String SMTP_PROTOCOL="smtp";
	public final static String SMTP_USER="mail.smtp.user";
	public final static String SMTP_PASSWORD="mail.smtp.password";
	public final static String PROTOCOL="smtp";
	
}
