package es.valhalla.exception;

public enum ValhallaBusinessExceptionEnum {

	
	CONNECTION_TIMEOUT("valhalla0001","Unexpected error"),
	IDP_NOT_REGISTERED("Valhalla0002", "IDP service is not present in service registry");
	
	private String code;
	private String message;
	
	
	
	private ValhallaBusinessExceptionEnum(final String code,final String message) {
		this.code = code;
		this.message = message;
	}
	
	
	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}

	public ValhallaBusinessException getException() {
		final ValhallaError error= new ValhallaError(code, message);
		final ValhallaBusinessException exception = new ValhallaBusinessException(error);
		return exception;
	}
	
	
	public ValhallaBusinessException getException(final Throwable throwable) {
		final ValhallaError error= new ValhallaError(code, message,throwable);
		final ValhallaBusinessException exception = new ValhallaBusinessException(error);
		return exception;
	}
	
}
