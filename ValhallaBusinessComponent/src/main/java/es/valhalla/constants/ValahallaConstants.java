package es.valhalla.constants;

public class ValahallaConstants {
	
	public static final String MONGO_URL = "mongo.url";
	public static final String MONGO_DBNAME = "mongo.database.name";
	public static final String MONGO_USER = "mongo.user";
	public static final String MONGO_SECRET = "mongo.secret";

	public static final String URL_FRONT = "email.url.front";
	public static final String EMAIL_SUBJECT = "email.subject";

	public static final String DRIVE_ID = "drive.client.id";
	public static final String DRIVE_SECRET = "drive.client.secret";
	public static final String DRIVE_REFRESH = "drive.refresh.token";

	public static final String INSERT = "Inserting %1$s in collection %2$s";
	public static final String UPDATE = "Updating %1$s in collection %2$s";
	public static final String DELETE = "Deleting %1$s in collection %2$s";
	public static final String GET = "Querying  collection %1$s";
	
	public static final String HTTP_STATUS="statusCode";
	public static final String HTTP_RESPONSE="statusCode";

	public static final String WILDCARD="*";
	
	public static final String AUTH_SERVICE="Auth";

	public static final String VALHALLA_LOG_PATH="logfilepath";
	public static final String VALHALLA_LOG_NAME="logfilename";
	public static final String ID="id";

}
