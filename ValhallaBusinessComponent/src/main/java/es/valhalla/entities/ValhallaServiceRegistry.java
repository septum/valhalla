package es.valhalla.entities;

public class ValhallaServiceRegistry {

	private String method;
	private int port;
	private String servicePath;
	private String serviceAddress;

	public ValhallaServiceRegistry() {
	}

	public ValhallaServiceRegistry(String method, int port, String servicePath,String serviceAddress) {
		this.method = method;
		this.port = port;
		this.servicePath = servicePath;
		this.serviceAddress=serviceAddress;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getServicePath() {
		return servicePath;
	}

	public void setServicePath(String servicePath) {
		this.servicePath = servicePath;
	}

	public String getServiceAddress() {
		return serviceAddress;
	}

	public void setServiceAddress(String serviceAddress) {
		this.serviceAddress = serviceAddress;
	}

}
