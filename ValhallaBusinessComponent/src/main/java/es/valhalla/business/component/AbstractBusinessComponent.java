package es.valhalla.business.component;

import java.io.File;
import java.util.List;
import java.util.Map;

import es.valhalla.constants.ValahallaConstants;
import es.valhalla.data.access.mongodb.entities.BaseEntity;
import es.valhalla.data.access.mongodb.service.MongoService;
import es.valhalla.jwt.entities.JWTPayload;
import es.valhalla.jwt.entities.JWTRoleENUM;
import es.valhalla.jwt.entities.JWTStatusENUM;
import es.valhalla.jwt.service.JWTService;
import es.valhalla.rest.service.RestClient;
import es.valhalla.utils.configuration.application.service.ConfigurationApplicationService;
import es.valhalla.utils.email.EmailUtils;
import es.valhalla.utils.hash.HashUtils;
import es.valhalla.utils.jsonserializer.JsonSerializeUtils;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.web.client.HttpResponse;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public abstract class AbstractBusinessComponent {

	@Inject
	MongoService mongoService;

	@Inject
	JWTService jwtService;

	@Inject
	RestClient restClient;

	@Inject
	ConfigurationApplicationService configurationApplicationService;
	

	public <T extends BaseEntity> void insertMongo(final String url, final String user, final String secret,
			final String databaseName, final String collection, final T data, final Class<T> clazz) {

		mongoService.insert(url, user, secret, databaseName, collection, data, clazz);
	}

	public <T extends BaseEntity> void updateMongo(final String url, final String user, final String secret,
			final String databaseName, final String collection, final T data, final Class<T> clazz) {
		mongoService.update(url, user, secret, databaseName, collection, data, clazz);
	}

	public <T extends BaseEntity> void deleteMongo(final String url, final String user, final String secret,
			final String databaseName, final String collection, final String docId, final Class<T> clazz) {
		mongoService.delete(url, user, secret, databaseName, collection, docId, clazz);
	}

	public <T extends BaseEntity> List<T> getAllMongo(final String url, final String user, final String secret,
			final String databaseName, final String collection, final Class<T> clazz) {
		return mongoService.getAll(url, user, secret, databaseName, collection, clazz);

	}

	public <T extends BaseEntity> T getMongo(final String url, final String user, final String secret,
			final String databaseName, final String collection, final String docId, final Class<T> clazz) {
		return mongoService.get(url, user, secret, databaseName, collection, docId, clazz);
	}

	public <T extends BaseEntity> List<T> find(final String url, final String user, final String secret,
			final String databaseName, final String collection, final Class<T> clazz,
			final Map<String, Object> params) {
		return mongoService.find(url, user, secret, databaseName, collection, clazz, params);

	}

	public long count(final String url, final String user, final String secret, final String databaseName,
			final String collection, final Map<String, Object> params) {
		return mongoService.count(url, user, secret, databaseName, collection, params);
	}

	public <T extends BaseEntity> List<T> getAnalytics(final String url, final String user, final String secret,
			final String databaseName, final String collection, final Map<String, Object> paramsQuery,
			final Class<T> clazz, final String... fields) {
		return mongoService.getAnalytics(url, user, secret, databaseName, collection, paramsQuery, clazz, fields);
	}

	public String saveDocumentMongo(final String url, final String user, final String secret, final String databaseName,
			final String bucketName, final Map<String, Object> metadata, final File file) {
		return mongoService.storeDocument(url, user, secret, databaseName, bucketName, metadata, file);
	}

	public Map<String, byte[]> getDocumentMongo(final String url, final String user, final String secret,
			final String databaseName, final String bucketName, final String docId) {
		return mongoService.downloadDocument(url, user, secret, databaseName, bucketName, docId);
	}

	public void deleteDocumentMongo(final String url, final String user, final String secret, final String databaseName,
			final String bucketName, final String docId) {
		mongoService.deleteDocument(url, user, secret, databaseName, bucketName, docId);
	}

	public String generateToken(final JWTRoleENUM role) {
		return jwtService.generateToken(role);
	}

	public byte[] hashPBKDF2(final String data, final byte[] salt) {
		return HashUtils.hashPBKDF2(data, salt);
	}

	public String hashPBKDF2ToString(final String data, final byte[] salt) {
		return new String(HashUtils.hashPBKDF2(data, salt));
	}

	public byte[] generateSalt() {
		return HashUtils.generateSalt();
	}

	public JWTPayload getClaims(final String jwt) {

		return jwtService.getClaims(jwt);
	}

	public boolean isValidToken(final String token) {
		boolean isValidToken = false;

		switch (jwtService.validateToken(token)) {
		case VALID:
			isValidToken = true;

			break;
		case INVALID:
		case EXPIRED:
			isValidToken = false;
			break;
		}

		return isValidToken;
	}

	public JWTRoleENUM getClaimsRole(final String jwt) {
		return getClaims(jwt).getRole();

	}

	public String getExp(final String jwt) {
		return getClaims(jwt).getExp();

	}

	public JWTStatusENUM validateToken(final String jwt) {
		return jwtService.validateToken(jwt);
	}

	public String getStringProperty(final String appName, final String property) {
		return configurationApplicationService.getStringProperty(appName, property);
	}

	public boolean getBooleanProperty(final String appName, final String property) {
		return configurationApplicationService.getBooleanProperty(appName, property);
	}

	public double getDoubleProperty(final String appName, final String property) {
		return configurationApplicationService.getDoubleProperty(appName, property);
	}

	public int getIntegerProperty(final String appName, final String property) {
		return configurationApplicationService.getIntProperty(appName, property);
	}

	public String serialize(final Object object) {
		return JsonSerializeUtils.serialize(object);
	}

	public <T> T deserialize(final String json, final Class<T> clazz) {
		return JsonSerializeUtils.deserialize(json, clazz);
	}

	public <T> List<T> deserializeUntypedList(final String json, final Class<T> clazz) {
		return JsonSerializeUtils.deserializeUntypedList(json, clazz);
	}

	public void sendEmail(final String appName, final String recipientEmail, final String subject,
			final String contentText) {
		EmailUtils.sendEmail(recipientEmail, subject, contentText,
				configurationApplicationService.readProperties(appName));
	}

	public Future<HttpResponse<Buffer>> exceuteHttpRequest(final String endpoint, final int port,
			final Map<String, String> headers, final String body) {
		return restClient.executePostRequest(endpoint, port, headers, body, ValahallaConstants.HTTP_STATUS,
				ValahallaConstants.HTTP_RESPONSE);
	}

}
