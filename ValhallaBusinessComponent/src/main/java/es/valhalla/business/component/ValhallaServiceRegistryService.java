package es.valhalla.business.component;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;

import es.valhalla.entities.ValhallaServiceRegistry;
import es.valhalla.utils.jsonserializer.JsonSerializeUtils;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;

public class ValhallaServiceRegistryService {

	

	private final static ValhallaLogger LOGGER = ValhallaLoggerImpl.getInstance(ValhallaServiceRegistryService.class);

	public static void selfRegister(final Vertx vertx,final Map<String, List<ValhallaServiceRegistry>> serviceRegistry,
			final String serviceRegistryPath, final String serviceName) {

		vertx.fileSystem().exists(serviceRegistryPath, result -> {
			if (result.succeeded() && result.result()) {
				final String json = vertx.fileSystem().readFileBlocking(serviceRegistryPath).toJson().toString();
				Map<String, List<ValhallaServiceRegistry>> readRegistry = JsonSerializeUtils.deserialize(json,
						new TypeReference<Map<String, List<ValhallaServiceRegistry>>>() {
						});
				if (readRegistry != null) {
					readRegistry.replace(serviceName,serviceRegistry.get(serviceName));
					serviceRegistry.putAll(readRegistry);
				}
			}
			write(vertx,serviceRegistry, serviceRegistryPath, serviceName);
		});

	}

	private static void write(final Vertx vertx,final Map<String, List<ValhallaServiceRegistry>> serviceRegistry,
			final String serviceRegistryPath, final String serviceName) {
		vertx.fileSystem().writeFile(serviceRegistryPath,
				Buffer.buffer(JsonSerializeUtils.serialize(serviceRegistry)), result -> {
					if (result.succeeded()) {
						LOGGER.info("SERVICE {} REGISTERED SUCCESFULLY", serviceName);
					} else {
						LOGGER.error("SERVICE {} NOT REGISTERED ", serviceName);
					}
				});
	}


}
