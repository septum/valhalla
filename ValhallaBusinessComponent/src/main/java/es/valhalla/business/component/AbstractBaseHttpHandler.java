package es.valhalla.business.component;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.Response;

import es.valhalla.constants.ValahallaConstants;
import es.valhalla.rest.service.RestClient;
import es.valhalla.utils.jsonserializer.JsonSerializeUtils;
import es.valhalla.utils.logger.ValhallaLogger;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.handler.BodyHandler;
import jakarta.inject.Inject;

@ApplicationScoped
public abstract class AbstractBaseHttpHandler {

	@Inject
	RestClient restClient;

	public final static String INCOMING_REQUEST = "Incoming request registered: {}, method: {} origin: {} ";
	public final static String FAILED_REQUEST = "Incoming request failed: {}, method: {} origin: {} ";

	public String serialize(final Object object) {
		return JsonSerializeUtils.serialize(object);
	}

	public <T> T deserialize(final String json, final Class<T> clazz) {
		return JsonSerializeUtils.deserialize(json, clazz);
	}

	public <T> List<T> deserializeUntypedList(final String json, final Class<T> clazz) {
		return JsonSerializeUtils.deserializeUntypedList(json, clazz);
	}

	private void processHttpResponse(final RoutingContext ctx, final String jsonObject,
			final Map<String, String> headers, final int statusCode) {

		ctx.response().setStatusCode(statusCode);
		if (headers != null) {
			headers.forEach((key, value) -> {
				ctx.response().putHeader(key, value);
			});
		}

		if (jsonObject != null) {
			ctx.response().end(jsonObject);
		} else {
			ctx.response().end();
		}

	}

	public Route buildRoute(final Router router, final String routePath, final HttpMethod method, final String consumes,
			final String produces) {
		return router.route(routePath).method(method).consumes(consumes).produces(produces)
				.handler(BodyHandler.create().setBodyLimit(100));
	}

	public Route buildRoute(final Router router, final String routePath, final HttpMethod method,
			final String consumes) {
		return router.route(routePath).method(method).consumes(consumes).handler(BodyHandler.create());
	}
	
	public Route buildRouteMultipart(final Router router, final String routePath, final HttpMethod method,
			final String consumes, final String uploadPath) {
		return router.route(routePath).method(method).consumes(consumes).handler(BodyHandler.create().setUploadsDirectory(uploadPath));
	}

	public Route buildRouteEmpty(final Router router, final String routePath, final HttpMethod method,
			final String produces) {
		return router.route(routePath).produces(produces).method(method);
	}

	public Route buildRouteEmpty(final Router router, final String routePath, final HttpMethod method) {
		return router.route(routePath).method(method);
	}

	public void processSuccessHttpResponse(final RoutingContext ctx, final String jsonObject) {
		processHttpResponse(ctx, jsonObject, null, Response.Status.OK.getStatusCode());
	}

	
	public void processSuccessHttpResponse(final RoutingContext ctx) {
		processHttpResponse(ctx, null, null, Response.Status.OK.getStatusCode());

	}
	public void processSuccessFileResponse(final RoutingContext ctx, final String filepath) {
			ctx.response().sendFile(filepath);
	}

	public void processUnauthorizedResponse(final RoutingContext ctx) {
		ctx.response().setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode());
		ctx.response().end();
	}

	public void processBadRequestResponse(final RoutingContext ctx) {
		ctx.fail(Response.Status.BAD_REQUEST.getStatusCode());
		ctx.end();
	}

	public Future<HttpResponse<Buffer>> idpValidation(final String host,final String idpEndpoint, final int port, final String body) {
		if (body != null) {
			final Map<String, String> headers = new HashMap<>();
			headers.put("Content-Type", "text/plain");
			return restClient.executePostRequest(host,idpEndpoint, port, headers, body, ValahallaConstants.HTTP_STATUS,
					ValahallaConstants.HTTP_RESPONSE);
		} else {
			return null;
		}

	}

	public String getTokenFromHeader(final HttpServerRequest request) {
		final String authorizationHeader = request.getHeader("Authorization");
		if (authorizationHeader != null) {
			final String token = authorizationHeader.substring("Bearer".length()).trim();
			return token;
		}
		return authorizationHeader;
	}

	protected void unexpectedFailure(final RoutingContext ctx, final ValhallaLogger LOGGER) {
		
		LOGGER.error(FAILED_REQUEST, ctx.failure().getStackTrace(), ctx.request().method(), ctx.request().host());
		ctx.response().setStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
		ctx.response().end();
	}

	

	public String getSelfAddress() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			return null;
		}
	}
}
