package es.valhalla.rest.service;

import java.util.Map;

import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.web.client.HttpResponse;

public interface RestClient {

	
	
	Future<HttpResponse<Buffer>> executeGetRequest(String endpoint, int port, Map<String, String> headers, 
			String statusKey, String responseKey);

	Future<HttpResponse<Buffer>> executePostRequest(String endpoint, int port, Map<String, String> headers, String body,
			String statusKey, String responseKey);

	Future<HttpResponse<Buffer>> executePostRequest(String host, String endpoint, int port, Map<String, String> headers,
			String body, String statusKey, String responseKey);

}
