package es.valhalla.rest.service.impl;

import java.util.Map;

import es.valhalla.rest.service.RestClient;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.impl.headers.HeadersMultiMap;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;

public class RestClientImpl implements RestClient {

	private Vertx vertx;

	private final static ValhallaLogger LOGGER = ValhallaLoggerImpl.getInstance(RestClientImpl.class);

	@Override
	public Future<HttpResponse<Buffer>> executeGetRequest(final String endpoint, final int port,
			final Map<String, String> headers, final String statusKey, final String responseKey) {
		final WebClient client = WebClient.create(getVertx());
		final HeadersMultiMap headersMap = new HeadersMultiMap();
		headersMap.addAll(headers);
		return client.get(endpoint).putHeaders(headersMap).send();

	}

	@Override
	public Future<HttpResponse<Buffer>> executePostRequest(final String endpoint, final int port,
			final Map<String, String> headers, final String body, final String statusKey, final String responseKey) {
		final WebClient client = WebClient.create(getVertx());
		final HeadersMultiMap headersMap = new HeadersMultiMap();
		headersMap.addAll(headers);
		LOGGER.info("REST CLIENT EXECUTING POST REQUEST OVER: {} ", endpoint);
		return client.post(endpoint).port(port).putHeaders(headersMap).sendBuffer(Buffer.buffer(body));

	}

	@Override
	public Future<HttpResponse<Buffer>> executePostRequest(final String host, final String endpoint, final int port,
			final Map<String, String> headers, final String body, final String statusKey, final String responseKey) {
		final WebClient client = WebClient.create(getVertx());
		final HeadersMultiMap headersMap = new HeadersMultiMap();
		headersMap.addAll(headers);
		LOGGER.info("REST CLIENT EXECUTING POST REQUEST OVER: {} ", endpoint);
		return client.post(host,endpoint).port(port).putHeaders(headersMap).sendBuffer(Buffer.buffer(body));

	}

	private synchronized Vertx getVertx() {

		if (vertx == null) {
			vertx = Vertx.currentContext().owner();
		}

		return vertx;
	}
}
