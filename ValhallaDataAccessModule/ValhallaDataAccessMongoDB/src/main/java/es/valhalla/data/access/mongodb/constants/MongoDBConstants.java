/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.valhalla.data.access.mongodb.constants;

/**
 *
 * @author marsa
 */
public  class MongoDBConstants {
    
   public static final String DOCUMENT_IDENTIFIER="ID";
   public static final String DOCUMENT_VALUE="VALUE";
   public static final String DOCUMENT_EXIST="Document already exist in database with the provided id: {}";
   public static final String CONNECTION_INFO="Opening connection to host: %1$s and database:%2$s ";
   public static final String CONNECTION_TIMEOUT="Error while trying to connect to MongoDB {}";
   public static final String UNKNOWN_ERROR="Unknown error happened while trying operation MongoDB";
   public static final String OPERATION="Executing {} in collection {} over document id: {}";
   
   public static final String INSERT_OP="INSERT";
   public static final String GET_OP="GET";
   public static final String UPDATE_OP="UPDATE";
   public static final String DELETE_OP="DELETE";
   public static final String ALL="ALL";
   public static final String FILTERED="FILTERED";
   public static final String COUNT="COUNT";

   public static final String BSON_FILTER_ID="_id";
   
   
    
}
