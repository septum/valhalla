/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.valhalla.data.access.mongodb.connection.impl;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import java.util.concurrent.TimeUnit;

import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

import es.valhalla.data.access.mongodb.connection.MongoConnectionFactory;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;

/**
 *
 * @author marsa
 */
public class MongoConnectionFactoryImpl implements MongoConnectionFactory {
	
	private ValhallaLogger LOGGER =ValhallaLoggerImpl.getInstance(MongoConnectionFactoryImpl.class);

	@Override
	public MongoClient getConnection(final String url, final String user, final String secret, final String database) {
		final CodecRegistry pojoCodecRegistry = fromProviders(PojoCodecProvider.builder().automatic(true).build());
		final CodecRegistry codecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
				pojoCodecRegistry);
		MongoClientSettings clientSettings = null;

		if (user != null && secret != null) {
			clientSettings = MongoClientSettings.builder().applyToConnectionPoolSettings(builder -> {
				builder.maxConnectionIdleTime(5, TimeUnit.SECONDS);
				builder.maxConnectionLifeTime(10, TimeUnit.SECONDS);
				builder.minSize(20);
				builder.maxSize(40);

			}).applyConnectionString(new ConnectionString(url)).applyToSocketSettings(builder -> {
				builder.connectTimeout(3, TimeUnit.SECONDS);
				builder.readTimeout(5, TimeUnit.SECONDS);
			}).credential(MongoCredential.createCredential(user, database, secret.toCharArray()))
					.codecRegistry(codecRegistry).build();
		} else {
			clientSettings = MongoClientSettings.builder().applyToConnectionPoolSettings(builder -> {
				builder.maxConnectionIdleTime(5, TimeUnit.SECONDS);
				builder.maxConnectionLifeTime(10, TimeUnit.SECONDS);
				builder.minSize(20);
				builder.maxSize(40);
			}).applyConnectionString(new ConnectionString(url)).applyToSocketSettings(builder -> {
				builder.connectTimeout(3, TimeUnit.SECONDS);
				builder.readTimeout(5, TimeUnit.SECONDS);
			}).codecRegistry(codecRegistry).build();
		}
		LOGGER.info("Conectando mongodb: host: {} db: {}",url,database);
		return MongoClients.create(clientSettings);
	}

}
