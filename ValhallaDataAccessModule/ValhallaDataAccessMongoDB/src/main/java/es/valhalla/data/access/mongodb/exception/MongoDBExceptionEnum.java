package es.valhalla.data.access.mongodb.exception;

import es.valhalla.exception.ValhallaError;

public enum MongoDBExceptionEnum {

	
	CONNECTION_TIMEOUT("valhalladataaccessmongo0001","Connection timeout reached without connecting"),
	DOCUMENT_ALREADY_EXIST("valhalladataaccessmongo0002","Document with given id already exist"),
	ERROR_READING_FILE("valhalladataaccessmongo0003", "Error reading file bytes"),
	UNHANDLED_ERROR("valhalladataaccessmongo9999","Mongo general error happened");
	
	private String code;
	private String message;
	
	
	
	private MongoDBExceptionEnum(final String code,final String message) {
		this.code = code;
		this.message = message;
	}
	
	
	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public MongoDBException getException(final Throwable throwable) {
		final ValhallaError error= new ValhallaError(code, message,throwable);
		final MongoDBException exception = new MongoDBException(error);
		return exception;
	}
	
}
