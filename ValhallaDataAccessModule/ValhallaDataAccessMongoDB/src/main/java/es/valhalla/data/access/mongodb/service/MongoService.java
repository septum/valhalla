/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.valhalla.data.access.mongodb.service;

import java.io.File;
import java.util.List;
import java.util.Map;

import es.valhalla.data.access.mongodb.entities.BaseEntity;

/**
 *
 * @author marsa
 */
public interface MongoService {

	void intializeConnection(String url, String user, String secret, String databaseName);
	
	<T extends BaseEntity> void insert(String url,  String user,  String secret, String databaseName, String collectionName, T data, Class<T> clazz);

	<T extends BaseEntity> List<T> getAll(String url,  String user,  String secret, String databaseName, String collectionName, Class<T> clazz);

	<T extends BaseEntity> T get(String url,  String user,  String secret, String databaseName, String collectionName, String id, Class<T> clazz);

	<T extends BaseEntity> void delete(String url,  String user,  String secret, String databaseName, String collectionName, String id,
			Class<T> clazz);

	<T extends BaseEntity> void update(String url,  String user,  String secret, String databaseName, String collectionName, T data, Class<T> clazz);

	<T extends BaseEntity> List<T> find(String url,  String user,  String secret, String databaseName, String collectionName, Class<T> clazz,
			Map<String, Object> params);

	long count(String url,  String user,  String secret, String databaseName, String collectionName, Map<String, Object> params);

	<T extends BaseEntity>List<T> getAnalytics(String url,  String user,  String secret, String databaseName, String collectionName,
			Map<String, Object> paramsQuery,Class<T> clazz, String... fields);

	String storeDocument(String url, String user, String secret, String databaseName, 
			String bucketName, Map<String, Object> metadata, File file);

	Map<String,byte[]> downloadDocument(String url, String user, String secret, String databaseName,
			String bucketName, String docId);

	void deleteDocument(String url, String user, String secret, String databaseName, String bucketName, String docId);

	
	
	

}