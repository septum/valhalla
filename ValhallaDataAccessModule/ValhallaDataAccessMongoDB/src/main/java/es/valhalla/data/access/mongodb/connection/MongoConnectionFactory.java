/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.valhalla.data.access.mongodb.connection;

import com.mongodb.client.MongoClient;

/**
 *
 * @author marsa
 */
public interface MongoConnectionFactory {
    
    public MongoClient getConnection(String url,  String user,  String secret,  String database);
    
}
