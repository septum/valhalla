/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.valhalla.data.access.mongodb.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.MongoException;
import com.mongodb.MongoTimeoutException;
import com.mongodb.MongoWriteException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.mongodb.client.gridfs.GridFSDownloadStream;
import com.mongodb.client.gridfs.GridFSUploadStream;
import com.mongodb.client.gridfs.model.GridFSUploadOptions;
import com.mongodb.client.model.Projections;

import es.valhalla.data.access.mongodb.connection.MongoConnectionFactory;
import es.valhalla.data.access.mongodb.constants.MongoDBConstants;
import es.valhalla.data.access.mongodb.entities.BaseEntity;
import es.valhalla.data.access.mongodb.exception.MongoDBExceptionEnum;
import es.valhalla.data.access.mongodb.service.MongoService;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

/**
 *
 * @author marsa
 */
@ApplicationScoped
public class MongoServiceImpl implements MongoService {

	@Inject
	private MongoConnectionFactory connection;

	private MongoClient mongoclient;

	private ValhallaLogger LOGGER = ValhallaLoggerImpl.getInstance(MongoServiceImpl.class);

	@Override
	public void intializeConnection(final String url, final String user, final String secret,
			final String databaseName) {
		getMongoClient(url, user, secret, databaseName);
	}

	@Override
	public <T extends BaseEntity> void insert(final String url, final String user, final String secret,
			final String databaseName, final String collectionName, final T data, final Class<T> clazz) {
		try {
			final MongoClient mongoClient = getMongoClient(url, user, secret, databaseName);

			final MongoDatabase db = mongoClient.getDatabase(databaseName);
			final MongoCollection<T> collection = db.getCollection(collectionName, clazz);
			collection.insertOne(data);
		} catch (final MongoTimeoutException mte) {
			LOGGER.error(MongoDBConstants.CONNECTION_TIMEOUT, mte, databaseName);
			throw MongoDBExceptionEnum.CONNECTION_TIMEOUT.getException(mte);
		} catch (final MongoWriteException mge) {
			LOGGER.error(MongoDBConstants.DOCUMENT_EXIST, mge, data.getId());
			throw MongoDBExceptionEnum.DOCUMENT_ALREADY_EXIST.getException(mge);
		} catch (final MongoException e) {
			LOGGER.error(MongoDBConstants.UNKNOWN_ERROR, e);
			throw MongoDBExceptionEnum.UNHANDLED_ERROR.getException(e);
		}
	}

	@Override
	public <T extends BaseEntity> List<T> getAll(final String url, final String user, final String secret,
			final String databaseName, final String collectionName, final Class<T> clazz) {
		final List<T> dataRetrieved = new ArrayList<T>();

		try {
			final MongoClient mongoClient = getMongoClient(url, user, secret, databaseName);
			final MongoDatabase db = mongoClient.getDatabase(databaseName);
			final MongoCollection<T> collection = db.getCollection(collectionName, clazz);
			collection.find().into(dataRetrieved);
			LOGGER.info(MongoDBConstants.OPERATION, MongoDBConstants.GET_OP, collectionName, MongoDBConstants.ALL);

		} catch (final MongoTimeoutException mte) {
			LOGGER.error(MongoDBConstants.CONNECTION_TIMEOUT, mte, databaseName);
			throw MongoDBExceptionEnum.CONNECTION_TIMEOUT.getException(mte);
		} catch (MongoException e) {
			LOGGER.error(MongoDBConstants.UNKNOWN_ERROR, e);
			throw MongoDBExceptionEnum.UNHANDLED_ERROR.getException(e);
		}

		return dataRetrieved;
	}

	@Override
	public <T extends BaseEntity> void delete(final String url, final String user, final String secret,
			final String databaseName, final String collectionName, final String id, final Class<T> clazz) {

		try {
			final MongoClient mongoClient = getMongoClient(url, user, secret, databaseName);
			final MongoDatabase db = mongoClient.getDatabase(databaseName);
			final MongoCollection<T> collection = db.getCollection(collectionName, clazz);
			final Map<String, Object> params = new HashMap<String, Object>();
			params.put(MongoDBConstants.BSON_FILTER_ID, id);
			final Bson filter = new Document(params);
			collection.deleteOne(filter);
			LOGGER.info(MongoDBConstants.OPERATION, MongoDBConstants.DELETE_OP, collectionName, id);
		} catch (final MongoTimeoutException mte) {
			LOGGER.error(MongoDBConstants.CONNECTION_TIMEOUT, mte, databaseName);
			throw MongoDBExceptionEnum.CONNECTION_TIMEOUT.getException(mte);
		} catch (final MongoException e) {
			LOGGER.error(MongoDBConstants.UNKNOWN_ERROR, e);
			throw MongoDBExceptionEnum.UNHANDLED_ERROR.getException(e);
		}

	}

	public <T extends BaseEntity> void update(final String url, final String user, final String secret,
			final String databaseName, final String collectionName, T data, Class<T> clazz) {

		try {
			final MongoClient mongoClient = getMongoClient(url, user, secret, databaseName);
			final MongoDatabase db = mongoClient.getDatabase(databaseName);
			final MongoCollection<T> collection = db.getCollection(collectionName, clazz);
			final Map<String, Object> params = new HashMap<String, Object>();
			params.put(MongoDBConstants.BSON_FILTER_ID, data.getId());
			final Bson filter = new Document(params);
			collection.deleteOne(filter);
			collection.insertOne(data);
			LOGGER.info(MongoDBConstants.OPERATION, MongoDBConstants.UPDATE_OP, collectionName, data.getId());
		} catch (final MongoTimeoutException mte) {
			LOGGER.error(MongoDBConstants.CONNECTION_TIMEOUT, mte, databaseName);
			throw MongoDBExceptionEnum.CONNECTION_TIMEOUT.getException(mte);
		} catch (final MongoException e) {
			LOGGER.error(MongoDBConstants.UNKNOWN_ERROR, e);
			throw MongoDBExceptionEnum.UNHANDLED_ERROR.getException(e);
		}

	}

	@Override
	public <T extends BaseEntity> T get(final String url, final String user, final String secret,
			final String databaseName, final String collectionName, final String id, final Class<T> clazz) {
		T data = null;

		try {
			final MongoClient mongoClient = getMongoClient(url, user, secret, databaseName);
			final MongoDatabase db = mongoClient.getDatabase(databaseName);
			final MongoCollection<T> collection = db.getCollection(collectionName, clazz);
			final Map<String, Object> params = new HashMap<String, Object>();
			params.put(MongoDBConstants.BSON_FILTER_ID, id);
			final Bson filter = new Document(params);
			final MongoCursor<T> cursor = collection.find(filter).iterator();

			if (cursor.hasNext()) {
				data = cursor.next();
			} else {
				return null;
			}
			LOGGER.info(MongoDBConstants.OPERATION, MongoDBConstants.GET_OP, collectionName, MongoDBConstants.FILTERED);

		} catch (final MongoTimeoutException mte) {
			LOGGER.error(MongoDBConstants.CONNECTION_TIMEOUT, mte, databaseName);
			throw MongoDBExceptionEnum.CONNECTION_TIMEOUT.getException(mte);

		} catch (final MongoException e) {
			LOGGER.error(MongoDBConstants.UNKNOWN_ERROR, e);
			throw MongoDBExceptionEnum.UNHANDLED_ERROR.getException(e);
		}

		return data;
	}

	@Override
	public <T extends BaseEntity> List<T> find(final String url, final String user, final String secret,
			final String databaseName, final String collectionName, final Class<T> clazz,
			final Map<String, Object> params) {
		List<T> dataRetrieved = null;

		try {
			final MongoClient mongoClient = getMongoClient(url, user, secret, databaseName);
			dataRetrieved = new ArrayList<T>();
			final MongoDatabase db = mongoClient.getDatabase(databaseName);
			final MongoCollection<T> collection = db.getCollection(collectionName, clazz);
			final Bson filter = new Document(params);
			collection.find(filter).into(dataRetrieved);
			LOGGER.info(MongoDBConstants.OPERATION, MongoDBConstants.GET_OP, collectionName, MongoDBConstants.FILTERED);

		} catch (final MongoTimeoutException mte) {
			LOGGER.error(MongoDBConstants.CONNECTION_TIMEOUT, mte, databaseName);
			throw MongoDBExceptionEnum.CONNECTION_TIMEOUT.getException(mte);
		} catch (final MongoException e) {
			LOGGER.error(MongoDBConstants.UNKNOWN_ERROR, e);
			throw MongoDBExceptionEnum.UNHANDLED_ERROR.getException(e);
		}

		return dataRetrieved;
	}

	@Override
	public long count(final String url, final String user, final String secret, final String databaseName,
			final String collectionName, final Map<String, Object> params) {
		long count = 0;

		try {
			MongoClient mongoClient = getMongoClient(url, user, secret, databaseName);
			final MongoDatabase db = mongoClient.getDatabase(databaseName);
			final MongoCollection<Document> collection = db.getCollection(collectionName);
			final Bson filter = new Document(params);
			count = collection.countDocuments(filter);
			LOGGER.info(MongoDBConstants.OPERATION, MongoDBConstants.GET_OP, collectionName, MongoDBConstants.COUNT);

		} catch (MongoTimeoutException mte) {
			LOGGER.error(MongoDBConstants.CONNECTION_TIMEOUT, mte, databaseName);
			throw MongoDBExceptionEnum.CONNECTION_TIMEOUT.getException(mte);
		} catch (MongoException e) {
			LOGGER.error(MongoDBConstants.UNKNOWN_ERROR, e);
			throw MongoDBExceptionEnum.UNHANDLED_ERROR.getException(e);
		}

		return count;
	}

	@Override
	public <T extends BaseEntity> List<T> getAnalytics(final String url, final String user, final String secret,
			final String databaseName, final String collectionName, final Map<String, Object> params,
			final Class<T> clazz, final String... fields) {
		List<T> mapResult = null;
		try {
			final MongoClient mongoClient = getMongoClient(url, user, secret, databaseName);
			mapResult = new ArrayList<T>();
			final MongoDatabase db = mongoClient.getDatabase(databaseName);
			final MongoCollection<T> collection = db.getCollection(collectionName, clazz);
			if (params != null) {
				final Bson filter = new Document(params);
				collection.find(filter).projection(Projections.include(fields)).into(mapResult);
			} else {
				collection.find().projection(Projections.include(fields)).into(mapResult);

			}
		} catch (final MongoTimeoutException mte) {
			LOGGER.error(MongoDBConstants.CONNECTION_TIMEOUT, mte, databaseName);
			throw MongoDBExceptionEnum.CONNECTION_TIMEOUT.getException(mte);
		} catch (final MongoException e) {
			LOGGER.error(MongoDBConstants.UNKNOWN_ERROR, e);
			throw MongoDBExceptionEnum.UNHANDLED_ERROR.getException(e);
		}
		return mapResult;
	}

	@Override
	public String storeDocument(final String url, final String user, final String secret, final String databaseName,
			final String bucketName, final Map<String, Object> metadata, final File file) {
		final MongoClient mongoClient = getMongoClient(url, user, secret, databaseName);
		final MongoDatabase db = mongoClient.getDatabase(databaseName);
		final GridFSBucket gridFSBucket = GridFSBuckets.create(db, bucketName);
		final GridFSUploadOptions options = new GridFSUploadOptions().metadata(new Document(metadata));

		try (final GridFSUploadStream uploadStream = gridFSBucket.openUploadStream(file.getName(), options)) {
			uploadStream.write(Files.readAllBytes(file.toPath()));
			uploadStream.flush();
			LOGGER.info("Stored document {}", file.getName());
			return uploadStream.getObjectId().toString();
		} catch (final IOException e) {
			LOGGER.error("Error storing document {}", e);
			throw MongoDBExceptionEnum.ERROR_READING_FILE.getException(e);
		}

	}

	@Override
	public Map<String, byte[]> downloadDocument(final String url, final String user, final String secret,
			final String databaseName, final String bucketName, final String docId) {
		final MongoClient mongoClient = getMongoClient(url, user, secret, databaseName);
		final MongoDatabase db = mongoClient.getDatabase(databaseName);
		final GridFSBucket gridFSBucket = GridFSBuckets.create(db, bucketName);
		final Map<String, byte[]> fileData = new HashMap<>();
		try (final GridFSDownloadStream downloadStream = gridFSBucket.openDownloadStream(new ObjectId(docId))) {
			int fileLength = (int) downloadStream.getGridFSFile().getLength();
			byte[] file = new byte[fileLength];
			downloadStream.read(file);
			fileData.put(downloadStream.getGridFSFile().getFilename(), file);

		}catch (MongoException e) {
			LOGGER.error("Error retrieving document {}", e);
			throw MongoDBExceptionEnum.ERROR_READING_FILE.getException(e);		}
		return fileData;
	}

	@Override
	public void deleteDocument(final String url, final String user, final String secret, final String databaseName,
			final String bucketName, final String docId) {
		final MongoClient mongoClient = getMongoClient(url, user, secret, databaseName);
		final MongoDatabase db = mongoClient.getDatabase(databaseName);
		final GridFSBucket gridFSBucket = GridFSBuckets.create(db, bucketName);
		gridFSBucket.delete(new ObjectId(docId));
	}

	private synchronized MongoClient getMongoClient(final String url, final String user, final String secret,
			final String databaseName) {

		if (mongoclient == null) {
			mongoclient = connection.getConnection(url, user, secret, databaseName);
		}
		return mongoclient;

	}

}
