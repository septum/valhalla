/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.valhalla.data.access.jdbc.service;

import java.util.List;
import java.util.Map;

import es.valhalla.data.access.jdbc.jdbc.enums.TablesEnum;

/**
 *
 * @author marsa
 */
public interface JDBCService {

    public void insert(final TablesEnum table, final Map<String, Object> args);
    
    public <T> T selectSimple(final TablesEnum table, final Class<T> clazz, final int id);
    
      public <T> T selectComplex(final TablesEnum table, final Class<T> clazz, final List<String> fields, final String where);
}
