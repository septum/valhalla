/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.valhalla.data.access.jdbc.constants;

/**
 *
 * @author marsa
 */
public class JDBCConstants {
    
    public final static String INSERT_SENTENCE="insert into  %1$s ( %2$s ) values ( %3$s )"; 
    public final static String DELETE_SENTENCE="delete from  %1$s where id=%2$s"; 
    public final static String UPDATE_SENTENCE="update %1$s SET  %2$s WHERE  id=%3$s ";
    public final static String SELECT_SENTENCE_SIMPLE="select * from  %1$s WHERE  id=%2$s ";
    public final static String SELECT_SENTENCE_COMPLEX="select %1$s from  %2$s WHERE  %3$s ";
    public final static String SINGLE_QUOTE="'"; 
    public final static String DELIMITER=","; 
    
}
