/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.valhalla.data.access.jdbc.connection;

import java.sql.Connection;


/**
 *
 * @author marsa
 */
public interface JDBCConnectionFactory {
    
    public Connection getConnection();
    
}
