/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.valhalla.data.access.jdbc.service.impl;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.commons.beanutils.PropertyUtils;

import es.valhalla.data.access.jdbc.connection.JDBCConnectionFactory;
import es.valhalla.data.access.jdbc.constants.JDBCConstants;
import es.valhalla.data.access.jdbc.jdbc.enums.TablesEnum;
import es.valhalla.data.access.jdbc.service.JDBCService;

/**
 *
 * @author marsa
 */
public class JDBCServiceImpl implements JDBCService {

    @Inject
    JDBCConnectionFactory connectionFactory;

    private Connection connection;

    @Override
    public void insert(final TablesEnum table, final Map<String, Object> args) {
        try {
            connection = connection != null ? connection : connectionFactory.getConnection();
            StringBuilder sbColumns = new StringBuilder();
            StringBuilder sbValues = new StringBuilder();
            args.forEach((k, v) -> {
                sbColumns.append(k).append(JDBCConstants.DELIMITER);

                if (k instanceof String) {
                    sbValues.append(JDBCConstants.SINGLE_QUOTE).append(v).append(JDBCConstants.SINGLE_QUOTE).append(JDBCConstants.DELIMITER);
                } else {
                    sbValues.append(v).append(JDBCConstants.DELIMITER);
                }

            });
            String query = String.format(JDBCConstants.INSERT_SENTENCE, table.getTable(), sbColumns.toString().substring(0, sbColumns.toString().length() - 1), sbValues.toString().substring(0, sbValues.toString().length() - 1));
            PreparedStatement ps = connection.prepareStatement(query);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(JDBCServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void delete(final TablesEnum table, final int id) {
        try {
            connection = connection != null ? connection : connectionFactory.getConnection();
            String query = String.format(JDBCConstants.DELETE_SENTENCE, table.getTable(), id);
            PreparedStatement ps = connection.prepareStatement(query);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void update(final TablesEnum table, final Map<String, Object> args, final String id) {

        try {
            connection = connection != null ? connection : connectionFactory.getConnection();
            StringBuilder sbColumns = new StringBuilder();
            StringBuilder sbValues = new StringBuilder();

            args.forEach((k, v) -> {
                sbColumns.append(k).append(JDBCConstants.DELIMITER);

                if (v instanceof String) {
                    sbValues.append(JDBCConstants.SINGLE_QUOTE).append(v).append(JDBCConstants.SINGLE_QUOTE).append(JDBCConstants.DELIMITER);
                } else {
                    sbValues.append(v).append(JDBCConstants.DELIMITER);
                }

            });
            String query = String.format(JDBCConstants.UPDATE_SENTENCE, table.getTable(), sbColumns.toString().substring(0, sbColumns.toString().length() - 1), sbValues.toString().substring(0, sbValues.toString().length() - 1));
            PreparedStatement ps = connection.prepareStatement(query);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public <T> T selectSimple(final TablesEnum table, final Class<T> clazz, final int id) {
        T selectResult = null;
        try {
            connection = connection != null ? connection : connectionFactory.getConnection();
            String query = String.format(JDBCConstants.SELECT_SENTENCE_SIMPLE, table.getTable(), id);

            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            selectResult = clazz.newInstance();
            while (rs.next()) {
                buildFromQuery(selectResult, rs, null);
            }

        } catch (SQLException ex) {
            Logger.getLogger(JDBCServiceImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
            Logger.getLogger(JDBCServiceImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return selectResult;
    }

    @Override
    public <T> T selectComplex(final TablesEnum table, final Class<T> clazz, final List<String> fields, final String where) {
        T selectResult = null;
        try {
            connection= connection!=null ? connection : connectionFactory.getConnection();
            StringBuilder sbColumsToRetrieve = new StringBuilder();
            fields.forEach(field -> {
                sbColumsToRetrieve.append(field).append(JDBCConstants.DELIMITER);
            });
            String query = String.format(JDBCConstants.SELECT_SENTENCE_COMPLEX, sbColumsToRetrieve.toString().substring(0, sbColumsToRetrieve.toString().length() - 1), table.getTable(), where);
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            selectResult = clazz.newInstance();
            while (rs.next()) {
                buildFromQuery(selectResult, rs, fields);
            }

        } catch ( SQLException ex) {
            Logger.getLogger(JDBCServiceImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(JDBCServiceImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(JDBCServiceImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(JDBCServiceImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(JDBCServiceImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return selectResult;
    }

    private <T> void buildFromQuery(final T clazz, ResultSet rs, final List<String> args) throws SQLException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, InstantiationException {
        List<Field> fields;
        if (args != null) {
            fields = Arrays.asList(clazz.getClass().getDeclaredFields()).stream().filter(field -> args.contains(field.getName())).collect(Collectors.toList());
        } else {
            fields = Arrays.asList(clazz.getClass().getDeclaredFields());
        }

        for (Field field : fields) {
            switch (field.getType().getSimpleName()) {
                case "String":
                    PropertyUtils.setSimpleProperty(clazz, field.getName(), rs.getString(field.getName()));
                    break;
                case "double":
                    PropertyUtils.setSimpleProperty(clazz, field.getName(), rs.getDouble(field.getName()));
                    break;
                case "int":
                    PropertyUtils.setSimpleProperty(clazz, field.getName(), rs.getInt(field.getName()));
                    break;
                default:
                    PropertyUtils.setSimpleProperty(clazz, field.getName(), rs.getString(field.getName()));
                    break;
            }
        }

    }

}
