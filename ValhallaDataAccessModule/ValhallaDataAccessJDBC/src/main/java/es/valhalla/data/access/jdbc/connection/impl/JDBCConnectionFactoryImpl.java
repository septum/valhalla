/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.valhalla.data.access.jdbc.connection.impl;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import es.valhalla.data.access.jdbc.connection.JDBCConnectionFactory;

/**
 *
 * @author marsa
 */
public class JDBCConnectionFactoryImpl implements JDBCConnectionFactory {

    @Override
    public Connection getConnection() {
        Connection connection = null;

        try {
            Context ctx = new InitialContext();
            Context initCtx = (Context) ctx.lookup("java:/comp/env");
            DataSource datasource = (DataSource) initCtx.lookup("jdbc/tfm");
            connection = datasource.getConnection();
        } catch (NamingException | SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

}
