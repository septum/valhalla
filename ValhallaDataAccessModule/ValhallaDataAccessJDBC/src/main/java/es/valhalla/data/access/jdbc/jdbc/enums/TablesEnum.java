/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.valhalla.data.access.jdbc.jdbc.enums;

/**
 *
 * @author marsa
 */
public enum TablesEnum {
    EJEMPLO("testing");

    private String table;

    private TablesEnum(String table) {
        this.table = table;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

}
