package es.valhalla.jwt.constants;

public class JWTConstants {

	public static final String JWT_TYPE="JWT";
	public static final String ALGORITHM="HS256";
	public static final String JWT_DOT=".";
	public static final String JWT_ISSUER="HHRR";
	public static final String JWT_AUDIENCE="HHRRFRONT";
	public static final String JWT_SUB="HHRR";
	public static final String JWT_DATE_FORMAT="yyyy-MM-dd'T'HH:mm:ss.SSS";
	public static final String JWT_MAC="HmacSHA256";
	public static final String ALG_ERROR="Error generating cryptographic keys";
	public static final String KEY="";

	public static final int JWT_EXP_TIME=3;
	
}
