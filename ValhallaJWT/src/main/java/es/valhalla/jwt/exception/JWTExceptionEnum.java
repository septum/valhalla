package es.valhalla.jwt.exception;

import es.valhalla.exception.ValhallaError;
import es.valhalla.jwt.constants.JWTConstants;
public enum JWTExceptionEnum {

	
	NO_SUCH_ALG("valhallajwt0001","Alg selected doesnt seem to exist"),
	KEY_EXCEPTION("valhallajwt0002",JWTConstants.ALG_ERROR);
	
	private String code;
	private String message;
	
	
	
	private JWTExceptionEnum(final String code,final String message) {
		this.code = code;
		this.message = message;
	}
	
	
	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public JWTException getException(final Throwable throwable) {
		final ValhallaError error= new ValhallaError(code, message,throwable);
		final JWTException exception = new JWTException(error);
		return exception;
	}
	
}
