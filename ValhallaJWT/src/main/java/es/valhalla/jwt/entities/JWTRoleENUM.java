package es.valhalla.jwt.entities;

public enum JWTRoleENUM {

	ADMIN("admin"), USER("user"), RESET("reset");

	private String role;

	public String getRole() {
		return this.role;
	}

	private JWTRoleENUM(String role) {
		this.role = role;
	}
}
