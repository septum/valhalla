package es.valhalla.jwt.entities;

import es.valhalla.jwt.constants.JWTConstants;

public class JWTHeader {

	private String alg=JWTConstants.ALGORITHM;
	private String typ= JWTConstants.JWT_TYPE;
	;
	public String getAlg() {
		return alg;
	}
	public void setAlg(String alg) {
		this.alg = alg;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	
	
	
	
	
}

