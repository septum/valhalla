package es.valhalla.jwt.entities;

public enum JWTStatusENUM {

	VALID("ok"), EXPIRED("expired"), INVALID("invalid");

	private String status;

	public String getStatus() {
		return this.status;
	}

	private JWTStatusENUM(String status) {
		this.status = status;
	}
}
