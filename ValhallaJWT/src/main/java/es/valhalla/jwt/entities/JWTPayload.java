package es.valhalla.jwt.entities;

public class JWTPayload {

		
	private String iss;
	private String exp;
	private String sub;
	private String aud;
	private JWTRoleENUM role;
	
	public String getIss() {
		return iss;
	}
	public void setIss(String iss) {
		this.iss = iss;
	}
	public String getExp() {
		return exp;
	}
	public void setExp(String exp) {
		this.exp = exp;
	}
	public String getSub() {
		return sub;
	}
	public void setSub(String sub) {
		this.sub = sub;
	}
	public String getAud() {
		return aud;
	}
	public void setAud(String aud) {
		this.aud = aud;
	}
	public JWTRoleENUM getRole() {
		return role;
	}
	public void setRole(JWTRoleENUM role) {
		this.role = role;
	}
	
	
	
	
}
