/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.valhalla.jwt.service;

import es.valhalla.jwt.entities.JWTPayload;
import es.valhalla.jwt.entities.JWTRoleENUM;
import es.valhalla.jwt.entities.JWTStatusENUM;

/**
 *
 * @author marsa
 */
public interface JWTService {
    
   public String generateToken(JWTRoleENUM role);
   
   public JWTStatusENUM validateToken(String jwt);
   
   public JWTPayload getClaims(String jwt);
    
}
