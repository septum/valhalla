package es.valhalla.jwt.service.impl;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.enterprise.context.ApplicationScoped;

import es.valhalla.jwt.constants.JWTConstants;
import es.valhalla.jwt.entities.JWTHeader;
import es.valhalla.jwt.entities.JWTPayload;
import es.valhalla.jwt.entities.JWTRoleENUM;
import es.valhalla.jwt.entities.JWTStatusENUM;
import es.valhalla.jwt.exception.JWTExceptionEnum;
import es.valhalla.jwt.service.JWTService;
import es.valhalla.utils.base64.Base64Utils;
import es.valhalla.utils.date.utils.DateUtils;
import es.valhalla.utils.jsonserializer.JsonSerializeUtils;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;

/**
 *
 * @author marsa
 */
@ApplicationScoped
public class JWTServiceImpl implements JWTService {

	private ValhallaLogger LOGGER = ValhallaLoggerImpl.getInstance(JWTServiceImpl.class);

	@Override
	public String generateToken(final JWTRoleENUM role) {
		String header = tokenize(new JWTHeader());
		String payload = buildPayload(role);
		String signature = hmacSha256(header.concat(JWTConstants.JWT_DOT).concat(payload),
				JWTConstants.KEY);
		return buildJWT(header, payload, signature);
	}

	@Override
	public JWTStatusENUM validateToken(final String jwt) {
		String[] parts = jwt.split("\\.");
		JWTPayload payload = JsonSerializeUtils.deserialize(Base64Utils.decode(parts[1]), JWTPayload.class);
		if (validateTokenExp(payload.getExp())) {
			String signature = parts[2];
			if (signature.equals(hmacSha256(parts[0].concat(JWTConstants.JWT_DOT).concat(parts[1]),
					JWTConstants.KEY))) {
				return JWTStatusENUM.VALID;
			} else {
				return JWTStatusENUM.INVALID;
			}

		} else {
			return JWTStatusENUM.EXPIRED;
		}

	}

	@Override
	public JWTPayload getClaims(final String jwt) {
		final String[] parts = jwt.split("\\.");
		final JWTPayload payload = JsonSerializeUtils.deserialize(Base64Utils.decode(parts[1]), JWTPayload.class);
		return payload;
	}

	private String tokenize(final Object jwt) {
		final String Json = JsonSerializeUtils.serialize(jwt);
		return Base64Utils.encode(Json);
	}

	private String buildPayload(final JWTRoleENUM role) {
		final JWTPayload payload = new JWTPayload();
		payload.setIss(JWTConstants.JWT_ISSUER);
		payload.setAud(JWTConstants.JWT_AUDIENCE);
		payload.setSub(JWTConstants.JWT_SUB);
		payload.setRole(role);
		payload.setExp(tokenExpTime());
		return tokenize(payload);
	}

	private String hmacSha256(final String data, final String secret) {
		try {
			final byte[] hash = secret.getBytes(StandardCharsets.UTF_8);
			final Mac sha256Hmac = Mac.getInstance(JWTConstants.JWT_MAC);
			final SecretKeySpec secretKey = new SecretKeySpec(hash, JWTConstants.JWT_MAC);
			sha256Hmac.init(secretKey);

			final byte[] signedBytes = sha256Hmac.doFinal(data.getBytes(StandardCharsets.UTF_8));

			return Base64Utils.encode(signedBytes);

		} catch (NoSuchAlgorithmException e)  {
			LOGGER.error(JWTConstants.ALG_ERROR, e);
			throw JWTExceptionEnum.NO_SUCH_ALG.getException(e);
		}catch (InvalidKeyException e) {
			LOGGER.error(JWTConstants.ALG_ERROR, e);
			throw JWTExceptionEnum.KEY_EXCEPTION.getException(e);
		}
	}

	private String buildJWT(final String header, final String payload, final String signature) {
		StringBuilder sb = new StringBuilder();
		sb.append(header).append(JWTConstants.JWT_DOT).append(payload).append(JWTConstants.JWT_DOT).append(signature);
		return sb.toString();
	}

	private String tokenExpTime() {
		SimpleDateFormat sdf = new SimpleDateFormat(JWTConstants.JWT_DATE_FORMAT);
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.HOUR_OF_DAY, JWTConstants.JWT_EXP_TIME);
		cal.getTime();
		return sdf.format(cal.getTime());
	}

	private boolean validateTokenExp(final String tokenExpDate) {

		return DateUtils.isDateBefore(DateUtils.getCurrentDate(JWTConstants.JWT_DATE_FORMAT), tokenExpDate,
				JWTConstants.JWT_DATE_FORMAT);

	}

}
