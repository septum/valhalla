package es.valhalla.drive.service;

import java.io.File;
import java.io.InputStream;

import es.valhalla.drive.entities.DriveObject;

public interface DriveService {

	InputStream downloadFile(final String objectId,final  String clientId,final  String clientSecret,
			 String refreshToken);

	DriveObject uploadFile(final String clientId, final String clientSecret, final String refreshToken,final  File file,
			String... parents);
	
	void deleteFile( final String clientId,final  String clientSecret,final  String refreshToken,
			final String fileId);

}
