package es.valhalla.drive.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;



import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.FileList;

import es.valhalla.drive.constants.GDriveConstants;
import es.valhalla.drive.entities.DriveObject;
import es.valhalla.drive.entities.GoogleJWT;
import es.valhalla.drive.service.DriveService;
import es.valhalla.rest.service.RestClient;
import es.valhalla.utils.jsonserializer.JsonSerializeUtils;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class DriveServiceImpl implements DriveService {

	private GoogleJWT jwt;
	
	private ValhallaLogger LOGGER= ValhallaLoggerImpl.getInstance(DriveServiceImpl.class);
	
	@Inject
	RestClient restClient;

	private Drive getDriveService(final String clientId, final String clientSecret, final String refreshToken)
			throws IOException, GeneralSecurityException {
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		final Drive service = new Drive.Builder(HTTP_TRANSPORT, JacksonFactory.getDefaultInstance(),
				getCredentials(clientId, clientSecret, refreshToken)).build();
		return service;
	}

	public InputStream downloadFile(final String objectId, final String clientId, final String clientSecret,
			final String refreshToken) {
		InputStream is = null;

		try {
			Drive service = getDriveService(clientId, clientSecret, refreshToken);
			is = service.files().get(objectId).executeMediaAsInputStream();
		} catch (GeneralSecurityException e) {
			LOGGER.error(GDriveConstants.ACCESS_ERROR,e);
		} catch (IOException e) {
			LOGGER.error(GDriveConstants.NOT_FOUND, e);

		}

		return is;
	}

	public DriveObject uploadFile(final String clientId, final String clientSecret, final String refreshToken,
			final File file, String... parents) {
		DriveObject drive = null;
		try {

			Drive service = getDriveService(clientId, clientSecret, refreshToken);

			com.google.api.services.drive.model.File body = new com.google.api.services.drive.model.File();
			body.set(GDriveConstants.NAME_ATTR, file.getName());

			String parentId = findFolder(service, parents[0]);
			List<String> parentsList = new ArrayList<String>();

			if (parentId == null) {
				parentsList.add(generateStructure(parentId, parents[0], service, body));

			} else {
				parentsList.add(parentId);
			}
			body.setParents(parentsList);
			String mimeType = Files.probeContentType(file.toPath());
			FileContent mediaContent = new FileContent(mimeType, file);
			com.google.api.services.drive.model.File fileResponse = service.files().create(body, mediaContent)
					.setFields("parents,id, name").execute();
			drive = new DriveObject();
			drive.setId(fileResponse.getId());
			drive.setName(fileResponse.getName());
		} catch (GeneralSecurityException e) {
			LOGGER.error(GDriveConstants.ACCESS_ERROR,e);
		} catch (IOException e) {
			LOGGER.error(GDriveConstants.NOT_FOUND, e);

		}
		return drive;
	}

	
	public void deleteFile(final String clientId, final String clientSecret, final String refreshToken,
			final String fileId) {
		try {
			Drive service = getDriveService(clientId, clientSecret, refreshToken);
			service.files().delete(fileId).execute();
		} catch (GeneralSecurityException e) {
			LOGGER.error(GDriveConstants.ACCESS_ERROR,e);
		} catch (IOException e) {
			LOGGER.error(GDriveConstants.NOT_FOUND, e);
		}

	}
	

	private String generateStructure(final String parentId, final String parentName, final Drive service,
			final com.google.api.services.drive.model.File body) throws IOException {

		com.google.api.services.drive.model.File fileMetadata = new com.google.api.services.drive.model.File();
		fileMetadata.setName(parentName);
		fileMetadata.setMimeType(GDriveConstants.MIME);
		String folderID = service.files().create(fileMetadata).execute().getId();

		return folderID;
	}

	private Credential getCredentials(final String clientId, final String clientSecret, final String refreshToken)
			throws IOException {

		Credential credential = new GoogleCredential.Builder().setClientSecrets(clientId, clientSecret)
				.setJsonFactory(JacksonFactory.getDefaultInstance()).setTransport(new NetHttpTransport()).build()
				.setAccessToken(getToken(clientId, clientSecret, refreshToken).getAccess_token()).setRefreshToken("");
		return credential;
	}

	private GoogleJWT generateToken(final String clientId, final String clientSecret, final String refreshToken)
			throws IOException {

		final String host = buildHost(clientId, clientSecret, refreshToken);
		final String gToken ="";//restClient.executeGetRequest(host, null);
		jwt = JsonSerializeUtils.deserialize(gToken, GoogleJWT.class);
		jwt.setRequestedAt(System.currentTimeMillis());

		return jwt;
	}

	private String buildHost(final String clientId, final String clientSecret, final String refreshToken) {
		StringBuilder sb = new StringBuilder(GDriveConstants.BASE_URL);

		sb.append(GDriveConstants.CLIENT_ID).append(GDriveConstants.EQUALS_SIGN).append(clientId)
				.append(GDriveConstants.APPENDER);

		sb.append(GDriveConstants.CLIENT_SECRET).append(GDriveConstants.EQUALS_SIGN).append(clientSecret)
				.append(GDriveConstants.APPENDER);

		sb.append(GDriveConstants.REFRESH_TOKEN).append(GDriveConstants.EQUALS_SIGN).append(refreshToken)
				.append(GDriveConstants.APPENDER);

		sb.append(GDriveConstants.GRANT_TYPE);

		return sb.toString();
	}

	private GoogleJWT getToken(final String clientId, final String clientSecret, final String refreshToken)
			throws IOException {

		long current = System.currentTimeMillis();

		if (jwt == null || current < jwt.getRequestedAt() + jwt.getExpires_in()) {
			jwt = generateToken(clientId, clientSecret, refreshToken);
			return jwt;
		} else {
			return jwt;
		}

	}

	private String findFolder(final Drive service, final String id) throws IOException {

		String pageToken = null;
		String foundID = null;
		do {
			FileList result = service.files().list().setQ(GDriveConstants.MIME_QUERY)
					.setFields(GDriveConstants.FIELDS_QUERY).setPageToken(pageToken).execute();
			for (com.google.api.services.drive.model.File file : result.getFiles()) {
				if (file.getName().equals(id)) {
					foundID = file.getId();
					pageToken = null;
				}
			}
			pageToken = result.getNextPageToken();
		} while (pageToken != null);
		return foundID;
	}



	
}
