package es.valhalla.drive.constants;

public class GDriveConstants {

	public final static String BASE_URL="https://oauth2.googleapis.com/token?";
	public final static String NAME_ATTR ="name";
	public final static String CLIENT_ID="client_id";
	public final static String CLIENT_SECRET="client_secret";
	public final static String APPENDER="&";
	public final static String REFRESH_TOKEN="refresh_token";
	public final static String GRANT_TYPE="grant_type=refresh_token";
	public final static String EQUALS_SIGN="=";
	public final static String ACCESS_ERROR="Error accesing gdrive";
	public final static String NOT_FOUND="File not found in gdrive";
	public final static String MIME="application/vnd.google-apps.folder";
	public final static String MIME_QUERY="mimeType = 'application/vnd.google-apps.folder'";
	public final static String FIELDS_QUERY="nextPageToken, files(id, name)";
	
}
